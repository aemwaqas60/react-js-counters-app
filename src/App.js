import React, {Fragment} from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './components/nav-bar';
import Counters from './components/counters'

function App() {
    return (

        <React.Fragment>
            <NavBar></NavBar>

            <div className="container">
                <Counters/>

            </div>

        </React.Fragment>
    );
}

export default App;
