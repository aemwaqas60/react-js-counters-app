import React, {Component} from 'react';
import Counter from "./counter";

class Counters extends Component {

    state = {
        items: [
            {id: 1, value: 0},
            {id: 2, value: 2},
            {id: 3, value: 1},
            {id: 4, value: 3},
            {id: 5, value: 0},
            {id: 6, value: 21},
            {id: 7, value: 12},
            {id: 8, value: 9},
            {id: 9, value: 0},
        ]
    };

    onDeleteHandler = counter => {

        const items = this.state.items.filter(c => c !== counter);
        this.setState({items})

    }

    onAddHandler = counter => {

        const items = [...this.state.items];
        const index = items.indexOf(counter);

        items[index] = {...counter};

        items[index].value++;
        this.setState({items})

    }

    onSubHandler = counter => {

        const items = [...this.state.items];
        const index = items.indexOf(counter);

        items[index].value--;
        this.setState({items})

    }

    onResetHandler = () => {
        const items = this.state.items.map(item => {
         item.value = 0;
         return item
    }
    )
        ;

        this.setState({items});
    }

    render() {

        return (
            <div>
                <button className="btn btn-success" onClick={() => this.onResetHandler()}>Reset</button>

                {
                    this.state.items.map(counter => (

                        <Counter
                            key={counter.id}
                            counter={counter}
                            onDelete={this.onDeleteHandler}
                            onAdd={this.onAddHandler}
                            onSub={this.onSubHandler}
                        />

                    ))
                }

            </div>

        )
    }


}

export default Counters;
