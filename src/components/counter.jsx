import React, {Component} from 'react';


class Counter extends Component {
    render() {

        const {counter, onDelete, onAdd, onSub} = this.props;

        return (


            <div className="row">


                <div className="col-1 m-2">

                    <button className="btn btn-primary">{counter.value}</button>

                </div>

                <div className="col">

                    <button className="btn btn-secondary m-2" onClick={() => onAdd(counter)}>+</button>

                    <button className="btn btn-secondary" onClick={() => onSub(counter)}
                            disabled={(counter.value === 0) ? "disabled" : ""}>-
                    </button>

                    <button className="btn btn-danger m-2" onClick={() => onDelete(counter)}

                    >Delete
                    </button>
                </div>


            </div>

        );
    }


}

export default Counter;
